#!/bin/bash
#Решает проблему с глюками сортировки в pdf ImageMagick
mkdir converted

for file in *.JPG;
do
	echo "Обработка и сжатие " $file "..."
	
	convert -contrast -contrast -quality 100 $file converted/$file'_edited.jpg'
	
	c44 -dpi 300 -percent 4 converted/$file'_edited.jpg' converted/$file'_result.djvu'
done

echo "Сборка .djvu..."
djvm -c "${PWD##*/}".djvu converted/*.djvu

echo "Удаление промежуточных файлов"
rm -r converted

echo "Готово"
