# Обозначения:

 - ПК - потенциальный ключ
 - PK - Primary Key
 - AK - Alternative Key

# Введение

 **БД** - программно-техническая система, предназначенная для хранения, модификации и предоставления пользователю информации.

 **СУБД** - это программная система, предназначенна для создания БД, изменения их структуры, обеспечения безопасности БД и для поддержания БД в актуальном (работоспособном) состоянии.

Большинство СУБД предполагает хранение информации в виде таблиц.

Для использования в БД эти таблицы должны обладать рядом свойств.

 1. Каждая таблица должна иметь имя, уникальное в пределах БД.
 2. Каждый столбец таблицы должен иметь имя, уникальное в пределах таблицы.
 3. Порядок строк и столбцов произвольный.
 4. Данные в столбцах должны быть однотипны.
 5. Данные в ячейках должны быть атомарны (в ячейках не могут содержаться множественные данные).
 6. Дублирование столбцов с однотипной информацией не допускается.
 7. Дублирование данных в столбцах не рекомендуется, т.к. может привести к нарушению целостности данных.
 8. Каждая таблица должна содержать первичный ключ (таблица не может содержать одинаковых записей).

В СУБД предусмотрен механизм типа данных.
Типы данных в разных СУБД отличаются, но классы типов данных одинаковы.

**Классы типов данных:**

 - Символьные (~2-4 КиБ)
	 - Varchar2(n)
	 - Char(n)
	 - Nchar(n)
 - Числовые
	 - Number(p,s) (precision - общее max число цифр, scale - в дробной части). p до 38
	 - Number(p) - целое
	 - Number <==> Number(38). **You've been warned.**
 - Дата/время
	 - Date: дата и время с точностью до секунды, 4713 до н.э. .. 31.12.9999.
	 - TimeStamp - до наносекунды.
 - Двоичные
	 - BLOB (4GiB)
	 - BFile (в файле ОС)
 - Большие символьные данные
	 - CLOB

 **Суперключ** - один или несколько столбцов таблицы, которые позволяют однозначно отличить одну запись таблицы от другой.

 **Потенциальный ключ** - минимальный набор столбцов, который позволяет отличить одну строку таблицы от другой.

  **Минимальный набор** - такой набор, из которого нельзя исключить ни один столбец без потери свойства однозначного определения записи.

В некоторых задачах потенциальный ключ будет содержать несколько столбцов.

Если ПК состоит из одного столбца, то он называется **простым ПК**.
Если из нескольких - **сложным** (композитным, составным).

 Тот ПК, который выбран для реализации таблицы называется **первичным** (PK).
В таблице может быть только один первичный ключ.

 Те ПК, которые не выбраны в качестве первичного ключа называются **альтернативными** (AK).

Основные **правила выбора** первичного ключа из набора потенциальных:

 1. Как можно меньше столбцов
 2. Желательно, чтобы столбцы PK были числовыми
 3. Желательно, чтобы объем данных, хранящийся в ячейках PK был как можно меньше
 4. В качестве столбцов PK не должны выбиратся столбцы, значения в которых могут измениться со временем
 5. Столбцы PK не должны содержать пустых ячеек

 Таблица, в которой связь проходит от столбцов PK (или AK) называется **главной** таблицей в этой связи. Вторая таблица называется **подчиненной**.

 Тот столбец или те столбцы, к которым подходит связь в подчиненной таблице называется(ются) **вторичным ключом (FK)**.

##Назначение связей

Связи предназначены для обеспечения ссылочной целостности БД.

 **Ссылочная целостность** - это такое свойство связи, которое

 1. Не позволяет удалить из главной таблицы записи, которые содержат в столбцах PK значения, встречающиеся среди значений FK подчиненной таблицы.
 2. Нельзя добавить в подчиненную таблицу запись, содержащую в столбике FK значение, отсутствующее среди значений PK в главной таблице.

Существует два **дополнительных режима** работы связей, по умолчанию отключенных:

 1. Режим каскадного удаления (delete cascade): при удалении записи из главной таблицы автоматически будут удалены все соответствующие записи в подчиненной таблице.
 2. Режим каскадного обновления (update cascade): при изменении значения PK в главной таблице, автоматически изменятся соответствующие FK в подчиненной.

 ключ, введенный искуственно называется **суррогатным**.

 Таблицы, обладающие перечисленными ранее восемью свойствами называются **отношениями**. БД на них основанные - реляционные.

Теорию реляционных БД предложил Edward Cogg (в 1970).

Также существуют **многомерные** БД: данные в таких базах могут быть представлены как ячейки многомерного куба.

В **NOSQL** данные хранятся в виде одной большой таблице, состоящей, как правило, из двух столбцов: ключа и значения.

**Объектные БД** - данные хранятся в виде объектов, обладающими свойствами и методами (Cache).

##Виды связей

 - Бинарные (соединяют две таблицы)
	 - 1 : 1 - такая связь между таблицами A и B, когда:
		 - Каждой записи в таблице A соответствует не более одной записи в таблице B
		 - Каждой записи в таблице B соответствует не более одной записи в таблице A
	 - 1 : M
		 - Каждой в A соответствует >= 0 из B
		 - Каждой из B не более одной из A
	 - М : 1 (наоборот)
	 - M : N
		 - Каждой из A соответствует >=0 из B
		 - Каждой из B соответствует >=0 из A
		 - Напрямую в большинстве реляционных СУБД не поддерживается, нужна ассоциативная таблица
			 - При простых ключах в A и B должна содержать по меньшей мере два столбца
			 - FK может быть частью PK
 - Унарные (соединяют одну таблицу саму с собой)
	 - Рекурсивная/рефлексивная - сама с собой напрямую
	 - Сетевая рекурсия - сама с собой через дополнительную ассоциативную таблицу


## Проектирование реляционных БД на основе модели сущность-связь

1976 Питер Чэл

### Основные понятия

 **Сущность** - именованный класс однотипных объектов, информация о которых должна быть учтена в модели.

 **Экземпляр сущности** - конкретный представитель данной сущности.

 **Атрибут** - именованная характеристика, являющаяся некоторым свойством сущности.

 **Первичный ключ сущности** - неизбыточный набор атрибутов, значения которых в совокупности являются уникальными для каждого экземпляра сущности.

 **Связь** - некоторая ассоциация между двумя сущностями. Одна сущность может быть связана с другими сущностями и сама с собой.

В отличие от реальных БД, между сущностями можно проводить M:N связи.

**Идентифицирующая связь** соединяет первичный ключ главной таблицы со вторичным ключом подчиненной таблицы, который является частью PK подчиненной таблицы. В этом случае подчиненная сущность называется зависимой, на схеме имеет скругленные углы.

**Неидентифицирующая связь** соединяет PK главной таблицы с FK подчиненной таблицы, атрибуты которого не входят в состав атрибутов первичного ключа. Такая связь отмечается пунктирной линией.

**Категориальная связь**

 - Полная: каждому экземпляру в супертипе обязательно соответствует экземпляр в каком либо подтипе.
 - Неполная: какому-либо экземпляру в супертипе не соответствует ни одного экземпляре в каком либо подтипе.

## Общие средства о современных CASE средствах

 - Rational Software Engineering: Rational Rose
 - Oracle Designer
 - Sybase: Power Designer
 - CA: All Fusion Modelling Suite
	 - ErWin DM, Validator, Process Modeller, Model Ward
	 - Forward/reverse engineering

### Validator

Ищет ошибки.

Группы ошибок:

 - Ошибки и недостатки моделирования колонок
	 - Inconsistent definition
		 - Одинаковые названия колонок в разных таблицах с разными типами данных
	 - Противоречивое определение групп колонок (Groups with i.d.)
	 - No columns in a table
	 - Duplicate table names
	 - Name length exceeds max
	 - Inconsistent default value (конфликт с типом данных или с check)
	 - Data names conflicting with SQL keywords
	 - Varchar columns with no length
 - -//- индексов и ограничений
	 - Missing index
	 - Колонки потенциальных ключей допускают null
	 - Таблица не имеет потенциального ключа
	 - Ненужные индексы
	 - AK with some nullable columns
 - -//- связей
	 - Incompatible relationships
	 - Infinite loops
	 - Crosslink relationships
 - Ошибки нормализации
	 - Нарушена первая нормальная форма
	 - Нарушена вторая нормальная форма
		 - Столбец обнаружен более чем в одной из связанных таблиц
	 - Нарушена третья нормальная форма

**Индексы** - специальные объекты БД, ускоряющие доступ к данным. Oracle поддерживает более 20 типов индексов.

 - B-Tree index
 - Bitmap index
 - Spatial index

Без индекса (линейный перебор) - **full scan**.

Индексом **не поддерживаются**

 - BLOB
 - CLOB
 - BFILE
 - ...

# Проектирование БД на основе теории нормальных форм

**Нормализация** - это процедура, в ходе которой создается оптимизированная структура базы данных, позволяющая избежать аномалий в БД.

**Аномалия** - такая ситуация в БД, которая приводит к противоречиям или существенно усложняет обработку данных.

**Три вида** аномалий:

 - Аномалия модификации данных - это такая ситуация, когда необходимость изменения одного значения в таблице приводит к необходимости просмотра всей таблицы.
 - Аномалия удаления данных - приводит к удалению информации, не связанной напрямую с удаляемыми данными.
 - Аномалия вставки данных - такая ситуация, когда информацию в таблицу нельзя поместить, пока информация неполная

Нормализация осуществляется путем **разбиения** таблицы на две или более таблицы, обладающих лучшими свойствами при вставке, изменении и удалении данных.

Алгоритм:

 - Перевод в 1NF
 - Перевод в 2NF
 - Перевод в 3NF
 - Перевод в нормальную форму Бойса-Кодда
 - Перевод в 4NF
 - Перевод в 5NF

**Пример:** пусть имеется организация, выполняющаяя некоторые проекты. 

 - Предположим, что сотрудники организации выполняют проекты, которые могут состоять из нескольких заданий.
 - Каждый сотрудник может участвовать в одном или нескольких проектах или временно не участвовать ни в одном.
 - Каждый сотрудник может выполнять только одно задание в проекте
 - Над каждым проектом может работать несколько сотрудников или он может быть временно приостановлен.
 - Над каждым заданием в проекте работает ровно один сотрудник.
 - Каждый сотрудник числится в одном отделе.
 - Каждый сотрудник имеет телефон, находящийся в отделе сотрудника (номер телефона один на весь отдел).
 - О каждом сотруднике надо хранить табельный номер (уник.) и фамилию.
 - Каждый отдел имеет номер (уник).
 - Каждый проект имеет номер (уник) и наименование.
 - Каждое задание из проекта имеет номер, уникальный в пределах проекта, т.е. задания в разных проектах могут иметь одинаковые номера).

### Первая нормальная форма

Если таблица удовлетворяет свойствам отношения, то она находится в первой нормальной форме.

**Функциональная зависимость**: Пусть R - отношение. Группа атрибутов Y функционально зависит от группы атрибутов X тогда и только тогда, когда для любого состояния отношения R для любых кортежей r1, r2: r1 ∈ R, r2 ∈ R из того, что r1.X=r2.X следует, что r1.Y=r2.Y.  
**Запись**: X → Y

X - детерминант функциональной зависимости, Y - зависимая часть.

Под **кортежем** мы будем понимать запись (строку) отношения.

Потенциальный ключ **всегда является детерминантом**.

### Вторая н.ф.

Отношение R находится во второй нормальной форме тогда и только тогда, когда R находится в 1NF и нет неключевых атрибутов, зависящих от части сложного ключа.
**Замечание**: если ПК отношения является простым, то отношение автоматически находится во второй нормальной форме.

Для того, чтобы устранить зависимость атрибутов от части сложного ключа, нужно произвести декомпозицию отношения на несколько отношений, при этом детерминант зависимости и те атрибуты, которые зависят от части сложного ключа, выносятся в отдельное отношение (атрибуты из исходного удаляются). 

### Третья нормальная форма

Отношение R находится в 3NF тогда и только тогда, когда оно находится во 2NF и все неключевые атрибуты взаимно независимы.

Для того, чтобы устранить зависимость неключевых атрибутов, нужно произвести декомпозицию отношения на несколько отношений. При этом зависимые неключевые атрибуты выносятся в отдельное отношение вместе с детерминантом отношения (из исходного эти атрибуты удаляются).

### Корректность процедуры нормализации. Декомпозиция без потерь.

Проекции A1 и A2 отнощения R называются декомпозицией без потерь, если отношение R точно восстанавливается из них при помощи естественного соединения для любого состояния отношения R.

Под естественным понимается соединение отношений по атрибутам с одинаковыми именами.

Пример 2:

 - Предположим, что сотрудники организации выполняют проекты, которые могут состоять из нескольких заданий.
 - Каждый сотрудник может участвовать в одном или нескольких проектах или временно не участвовать ни в одном.
 - Каждый сотрудник может выполнять только одно задание в проекте
 - Над каждым проектом может работать несколько сотрудников или он может быть временно приостановлен.
 - Над каждым заданием в проекте может работать несколько сотрудников

### НФ Бойса-Кодда

[missing] и любая функциональная зависимость имеет в качестве детерминанта ПК отношения.

Декомпозицию можно сделать по любой ф.з. не имеющей в качестве детерминанта ПК.

Проверять отношение на НФ Б-К надо только тогда, когда отношение имеет пересекающиеся ключи или несколько непересекающихся.

### Четвертая НФ

Пусть R - некое отношение и X, Y, Z - атрибуты или непересекающиеся множества атрибутов. Тогда Y и Z многозначно зависят от X ⇔ из того, что в отношении R содержатся кортежи r1(x, y, z1) и r2(x, y1, z) следует, что в отношении находится кортеж r3(x, y, z)
X ↠ Y/Z

Отношение R находится в **4NF** ⇔ R находится в НФ Б-К и не содержит многозначных зависимостей.

Приведение: декомпозиция на R1(X, Z) и R2(X, Y).  
Декомпозицию нужно проводить только если Y и Z взаимно независимы.

Многозначные зависимости возникают только тогда, когда все атрибуты входят в ключ.

### 5NF

Отношение находится в 5NF тогда, когда оно в 4NF и отсутствуют зависимости соединения.

Пусть R - отношение и содержит группы атрибутов S, P, Z. В этом отношении имеет место **зависимость соедиения** тогда, когда из условия, что в этом отношении находится три кортежа:
 - r1(s, p, z1)
 - r2(s, p1, z)
 - r3(s1, p, z)

следует наличие кортежа r4(s,p,z).


# Язык SQL

1975 Эдгар Кодд

Стандарт SQL (SQL0 .. SQL4) состоит из обязательной и рекомендательной частей.

Язык состоит из

 - Идентификаторов
 - Операторов
	 - Арифметические
	 - Сравнения
	 - Like 'шаблон'
		 - В шаблоне два метасимвола: 
			 - _ - любой
			 - % - любое количество любых символов (в т.ч. отсутствие)
 - Функции
 - Комментарии
 - Команды

Требования к идентификаторам:

 - Имя начинается с буквы латинского или национального алфавита
 - Внутри имени объекта могут использоваться буквы латинского или национального алфавитов, цифры и три спецсимвола: _ # $
 - Нельзя в качестве имени объекта использовать зарезервированные keywords
	 - Если очень хочется, то можно: используйте ограничители (по стандарту - двойные кавычки)
 - Количество символов в имени объекта ограничено: в Oracle <= 30 байт

### Функции

 1. Математические
 - Round(num, deg), deg может быть и отрицательной
 - Round(date, 'название части даты')
 - power, sin, mod, P(), etc.
 2. Символьные
 - Upper, Lower, Length, Substr, Instr
 - Note: чувствительны к регистру
 3. Функции даты и времени
 - sysdate
 - add_months
 - months_between
 4. Агрегатные
 - sum
 - avg
 - min/max
 - count
 - Note: большинство игнорирует null значения
 5. Преобразовательные
 - to_char
 - to_date

Комментарии: "--", "/* ... */

### Группы команд:

 - DDL - Data Definition Language: создание, изменение объектов, очистка таблиц и т.д.
	 - Create table %title (...)
	 - Alter
		 - Modify
	 - Drop
	 - Truncate table
 - DML - Data Manipulation Language
	 - Insert into %name values(...)
		 - Это однострочная команда
	 - Update
	 - Where
 - TCL (Transaction Control Language)
	 - Rollback
	 - Commit
 - DCL (Data Control Language)
	 - Grant (предоставление привилегий)
		 - Ex: GRANT CREATE TABLE TO Petrov
	 - Revoke (отзыв привилегий)
 - DQL (Data Query Language)
	 - Select

**Транзакция** - это набор команд (как правило, DML), которые выполняются как единое целое.

Каждая команда из DDL является транзакцией.

**Select**

    SELECT [DISTINCT] СписокСтолбцов
    FROM СписокТаблиц 
    WHERE Условия
    GROUP_BY
    HAVING
    ORDER_BY [ASK(default)|DESC];

Порядок следования разделов должен сохраняться.

Список столбцов = * | имя столбца | имя таблицы.имя столбца [AS псевдоним](* | имя столбца | имя таблицы.имя столбца)

В разделе WHERE агрегатные функции использовать нельзя.

### Извлечение данных из нескольких таблиц

**Внутреннее соединение двух таблиц**:

    SELECT Список_Столбцов
    FROM Имя_Таблицы_1 INNER JOIN Имя_Таблицы_2
	    ON Имя_Таблицы_1.Имя_Столбца = Имя_Таблицы_2.Имя_Столбца

Если в ON стоит знак равенства, то соединение называется соединением по эквивалентности.

--------

# Архитектура современных баз данных

**БД (альтернативное определение)** - набор файлов, который включает в себя

 - Файлы данных
 - Файлы управления: только системная информация
 - Файлы оперативных журналов повторов: информация о выполненных командах над данными

Специальная область оперативной памяти и фоновые процессы называются **экземпляром** базы данных (системная глобальная область, SGA).

Эта область делится на несколько частей:

 - DB buffers (временное хранение изменяемых данных)
 - Shared pool (коды скомпилированных программ и планы выполнения запросов)
 - Large pool
 - Java pool
 - Low buffer
 - Redo Log Buffer

Условия сброса информации в файлы фоновыми процессами (взаимодействие между SGA и файлами БД):

 - Раз в 3 секунды
 - При завершении транзакций
 - >1MB в RLB

Примеры этих процессов:

 - DB Writer: DB Buffers --> файлы данных
 - Log Writer: RLB --> файлы оперативных журналов повторов
 - CMon (control monitor): работает в основном при запуске БД, запускает все остальные фоновые процессы и, если надо, производит восстановление БД
 - PMon (process monitor): при остановке БД, закрывает остальные процессы, откатывает все незавершенные транзакцие, приводит БД в согласованное состояние

Экземпляр БД в совокупности с самой БД называется **сервером БД**.

[MISSING про клиентов]

**Dedicated Server** - режим, при котором каждому клиенту выделяется свой процесс.
**Shared Mode** - режим, при котором выделяется ограниченное количество серверных процессов
**Dispatcher** - управляет очередью пользователей

Данные из файлов в ОП записывают **серверные процессы**.

Параметры системы хранятся в **файле инициализации**.
Также существует **файл паролей** привилегированных учетных записей (имеющие право на запуск и остановку БД).
**Файлы архивов** - файлы, где хранятся архивы RLB

И многие другие.

## Хранение информации в файлах БД

Файлы данных объединяются в **группы** (Tablespace для Oracle, File Group для SQL Server и т.д.), являющиеся одной логической единицей для СУБД. Под объекты в табличном пространстве выделяется место - сегменты.

В случае нехватки места выделяется экстент, принадлежащий, в отличие от сегмента, одному физическому файлу.

В рамках каждого экстента выделяются блоки (страницы) - самая мелкая единица. Один блок предназначен для хранения одной или нескольких записей таблицы. Блоки в Oracle обычно имеют размер 2, 4, 8, 16 или 32 КБ. Размер блока устанавливается на табличное пространство.

Хранить одну запись в нескольких блоках неэффективно, поэтому СУБД при выборе размера блоков старается избежать этой ситуации.
